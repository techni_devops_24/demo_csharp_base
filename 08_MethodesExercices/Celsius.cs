﻿

namespace _08_Methodes
{
    public struct Celsius
    {
        public double Temperature;

        public double ToFahrenheit()
        {
            return Temperature * 9 / 5 + 32;
        }
    }
}
