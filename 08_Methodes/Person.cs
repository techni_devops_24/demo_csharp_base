﻿
namespace _08_Methodes
{
    public struct Person
    {
        public string LastName, FirstName;
        public DateOnly Birthdate;

        public Person(string ln, string fn, DateOnly bd)
        {
            LastName = ln;
            FirstName = fn;
            Birthdate = bd;
        }

        //Procédure
        public void Display()
        {
            Console.WriteLine($"Nous vous présentons {FirstName} {LastName} né(e) le {Birthdate.ToShortDateString()}");
        }

        //surcharge
        public void Display(string actualJob )
        {
            Console.WriteLine($"Nous vous présentons {FirstName} {LastName} né(e) le {Birthdate.ToShortDateString()} travaillant comme {actualJob}");
        }

        //surcharge
        public void Display(string actualJob, bool isHappy)
        {
            Console.WriteLine($"Nous vous présentons {FirstName} {LastName} né(e) le {Birthdate.ToShortDateString()} travaillant comme {actualJob} et { (isHappy ? "étant très heureux :)" : "n'étant pas du tout heureux :(") } dans son travail");
        }

        //Fonction
        public int Age()
        {
            return DateTime.Now.Year - Birthdate.Year;
        }

        //Avec condition
        public bool IsAdult()
        {
            if(Age() >= 18)
            {
                return true;
            }
            return false;

            //return Age() >= 18;
        }

        //Avec param (+ facultatif)
        public void SayHi(string lang = "fr")
        {
            if (Age() <= 2)
            {
                Console.WriteLine("Agagougou");
            }
            else
            {
                switch (lang)
                {
                    case "fr":
                        Console.WriteLine("Bonjour");
                        break;
                    case "en":
                        Console.WriteLine("Hello");
                        break;
                    case "it":
                        Console.WriteLine("Buongiorno");
                        break;
                    default:
                        Console.WriteLine("❣⛎☯🕎🔯💫♈💤💗");
                        break;
                }
            }
        }

        public void FavHobbies(string hobbyN1, string hobbyN2, string hobbyN3)
        {
            Console.WriteLine($"Les hobbies favoris de {FirstName} sont, dans l'ordre :\n1) {hobbyN1}\n2) {hobbyN2}\n3) {hobbyN3}");
        }

        //public void FavNumber(in int number)
        //{
        //    //Le mot-clef in sert à lock un paramètre, je ne peux pas modifier sa valeur
        //    //number += 12;//Nope
        //    Console.WriteLine(number);
        //}
        public void FavNumber(ref int number)
        {            
            number += 12;
            Console.WriteLine(number);
        }

        //Règles d'or des params
        //1) Toujours UN seul par méthode
        //2) Toujours en DERNIER dans les paramètres
        //3) On respecte LE type des params
        public void Jobs(bool retired, params string[] jobs) 
        {
            if(jobs.Length == 0)
            {
                Console.WriteLine($"{FirstName} est un chomeurvie");
            }
            else
            {
                Console.WriteLine($"{FirstName} a eu comme emploi(s) : ");
                foreach(string job in jobs)
                {
                    Console.WriteLine(job);
                }
            }

            Console.WriteLine($"{FirstName} est {(retired ?  "retraité" : "toujours en activité")}");

        }

    }
}
