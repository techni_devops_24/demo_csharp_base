﻿
namespace _08_MethodesExercices
{
    public struct E2D
    {
        public double A, B, C;

        public bool Resoudre(out double? X1, out double? X2)
        {
            double Delta = B * B - 4 * A * C;
            if(Delta < 0)
            {
                X1 = null;
                X2 = null;
                return false;
            }
            if(Delta == 0)
            {
                X2 = (-B) / (2 * A);
                X1 = X2;
                return true;
            }
            X1 = (-B + Math.Sqrt(Delta)) / (2 * A);
            X2 = (-B - Math.Sqrt(Delta)) / (2 * A);
            return true;

        }

    }

   
}
