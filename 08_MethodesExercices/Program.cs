﻿// 1) Dans les structures Celsius et Fahrenheit, écrire la fonction de conversion de l’une vers l’autre.
using _08_Methodes;
using _08_MethodesExercices;

Celsius tempIci;
tempIci.Temperature = 13.00;
Console.WriteLine($"Ici, il fait actuellement {tempIci.Temperature}°C, ce qui donne {tempIci.ToFahrenheit().ToString("F")}°F");

Fahrenheit tempLaba;
tempLaba.Temperature = 45.7;
Console.WriteLine($"Laba, il fait actuellement {tempLaba.Temperature}°F, ce qui donne {tempLaba.ToCelsius().ToString("F")}°C");

// 2) Ecrire une structure pour résoudre une équation du second degré.
// La structure devra contenir :
// * Trois variables membres publiques A, B et C de type double.
// * Une méthode publique « Resoudre » retournant une valeur de type « bool » stipulant si une réponse a été trouvée et devra retourner également les valeurs de X1 et de X2 de type double.
// * Si aucune solution n’a été trouvée, les valeurs de X1 et de X2 doivent être égale à « null »
Console.WriteLine();
E2D e2d1 = new E2D() { A = 15 , B = 0, C = 12};
if (e2d1.Resoudre(out double? X1, out double? X2))
{
    Console.WriteLine($"Résolution réussie : X1 = {X1} / X2 = {X2}");
}
else
{
    Console.WriteLine("Résolution ratée");
}

E2D e2d2 = new E2D() { A = 12, B = 0 , C = 0 };
if (e2d2.Resoudre(out double? X1bis, out double? X2bis))
{
    Console.WriteLine($"Résolution réussie : X1 = {X1bis} / X2 = {X2bis}");
}
else
{
    Console.WriteLine("Résolution ratée");
}

E2D e2d3 = new E2D() { A = 2, B = 12, C = 4 };
if (e2d3.Resoudre(out double? X1bisbis, out double? X2bisbis))
{
    Console.WriteLine($"Résolution réussie : X1 = {X1bisbis} / X2 = {X2bisbis}");
}
else
{
    Console.WriteLine("Résolution ratée");
}

// 3) Créer une structure représentant un Rectangle. Elle devra contenir
// Longueur, Largeur
// Une méthode pour calculer l'aire
// Créer une structure réprésentant un Cercle. Elle devra contenir
// Rayon
// Une méthode pour calculer l'aire
Console.WriteLine();
Rectangle rectangle = new Rectangle() { Largeur = 2, Longueur = 6 };
Console.WriteLine("L'aire du rectangle est : " + rectangle.CalculAire());

Cercle cercle = new Cercle() { Rayon = 10 };
Console.WriteLine("L'aire du cercle est : " + cercle.CalculAire());


