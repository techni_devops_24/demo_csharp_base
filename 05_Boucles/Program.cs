﻿// Rappel : 
// Quand est-ce que j'utilise un while ?
// Quand je ne connais pas le nombre d'itérations à faire, c'est interaction qui va nous faire sortir de la boucle
// ex : Menu, Jeux, Lecture(Stream, Reader etc)
// Quand est-ce que j'utilise un for ?
// Quand je connais le nombre d'itérations à faire
// ex : Tableaux, Listes

// while
//while (condition) {  traitement tant que condition vraie }
string userChoice = "";
while (userChoice != "Q")
{
    Console.WriteLine("Menu 1:");
    Console.WriteLine("Que souhaitez-vous faire ?\nA) Jouer\nB) Paramètres\nQ) Quitter");
    userChoice = Console.ReadLine();
}

// do ... while
string userChoice2; //Pas besoin d'init puisqu'on passe d'office une fois dans le do avant d'arriver dans le while
do
{
    Console.WriteLine("\nMenu 2:");
    Console.WriteLine("Que souhaitez-vous faire ?\nA) Jouer\nB) Paramètres\nQ) Quitter");
    userChoice2 = Console.ReadLine();
} while (userChoice2.ToUpper() != "Q");

// for
//for( initialisation compteur , condition de séjour , incrémentation/décrémentation )
for(int i= 0; i < 10; i++)
{
    Console.WriteLine(i+1);
}

int[] notes = [12, 2, 8, 3, 20];
string[] hobbies = ["Dessin", "Dev", "Twitch"];
// Pour parcourir :
// Avec le for classique 💤
for (int i=0; i < notes.Length; i++)
{
    Console.Write($"{ notes[i] } | ");
}

Console.WriteLine();
// Avec le foreach 🥳
// On récupère la valeur, donc la variable doit être du même type que les données dans le tableau
foreach (int note in notes)
{
    Console.Write($"{note} | ");

}
foreach(string hobby in hobbies)
{
    Console.Write($"{hobby} | ");
}