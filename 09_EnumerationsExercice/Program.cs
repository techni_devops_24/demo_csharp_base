﻿using _09_EnumerationsExercice;
// 1) Créer une énumération pour les couleurs (Coeur, Carreau, Pique, Trefle) ✅

// 2) Créer une énumération pour les valeurs (as = 14, deux = 2, trois = 3, ..., Roi = 13)
// ✅

// 3) Créer une structure Carte qui contient deux variables publiques :
// Couleur de type Couleurs
// Valeur de type Valeurs ✅

// 4) Déclarer un tableau de Carte d'une taille de 52
Carte[] cartes = new Carte[52];

// 5) À l'aide d'une boucle « foreach » définir les couleurs et les valeurs de chacune des cartes
int i = 0;
foreach(string valeur in Enum.GetNames(typeof(ValeurEnum)))
{
    foreach(string couleur in Enum.GetNames(typeof(CouleursEnum)))
    {
        Carte carte = new Carte() {
            Couleur = (CouleursEnum)Enum.Parse(typeof(CouleursEnum), couleur),
            Valeur = (ValeurEnum)Enum.Parse(typeof(ValeurEnum), valeur)
        };
        cartes[i++] = carte;
    }
}

// 6) Afficher les cartes (Définir si cela fonctionne : si oui pourquoi, sinon pourquoi)
foreach(Carte carte in cartes)
{
    Console.WriteLine($"{carte.Valeur} de {carte.Couleur}");
}