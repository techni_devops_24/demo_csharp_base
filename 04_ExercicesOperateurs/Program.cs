﻿// 1) Calcul de la division entière, du modulo et de la division de deux entiers.
//Résultat attendu pour 5 et 2 🡪 Division entière : 2, Modulo: 1, Division: 2,5.
using System.Collections.Generic;

Console.WriteLine("Exercice 1 :\n");
int x = 5;
int y = 2;
Console.WriteLine($"Division entière de {x} par {y} : { x/y } ");
Console.WriteLine($"Modulo de {x} par {y} : { x%y } ");
Console.WriteLine($"Division de {x} par {y} : { (double)x/y } ");

Console.WriteLine();
// 2) Vérification d’un compte bancaire BBAN, si le compte est bon affichez OK sur la console sinon KO.
// Le modulo des 10 premiers chiffres par 97 donne les 2 derniers. Sauf si le modulo = 0 dans ce cas les 2 derniers chiffres sont 97.
Console.WriteLine("Exercice 2 :\n");

string compte1 = "000000014245";
string compte2 = "000000014246";
string compte3 = "000000000097";

string compte = compte1;

if(int.TryParse(compte.Substring(0, 10), out int tenFirst) &&
    int.TryParse(compte.Substring(10), out int twoLast) )
{
    if (tenFirst % 97 == twoLast || (tenFirst % 97 == 0 && twoLast == 97) )
    {
        Console.WriteLine("Ok");
    }
    else
    {
        Console.WriteLine("Ko");
    }
}
else
{
    Console.WriteLine("Numero de compte invalide !");
}

Console.WriteLine();
// 3) Transformer un compte bancaire BBAN Belge (xxx-xxxxxxx-xx) en IBAN (BExx-xxxx-xxxx-xxxx). Trouvez la démarche via un moteur de recherche.
// TODO : IF ALGO AU DESSUS MARCHE ON CALCULE IBAN SINON ON FAIT RIEN BUT FLEMME (parce que là le compte2 passe alors qu'on devrait pas faire le calcul)
Console.WriteLine("Exercice 3 :\n");
// 1.Remove all non-alphanumerical characters:
string compteToConvert = compte1;
Console.WriteLine($"BBAN : {compteToConvert}");


// 2.Retrieve last two digits:
int.TryParse(compteToConvert.Substring(10), out int twoLastBis);
Console.WriteLine("Deux derniers : " + twoLastBis);

// 3.Create “dummy” by using the check digits, twice and converting BE into numbers and appending two zero’s:
// BE → B = 11, E = 14 → BE = 1114
string dummyAccount = $"{twoLastBis.ToString("D2")}{twoLastBis.ToString("D2")}111400";
Console.WriteLine("\"Dummy\" Account : " + dummyAccount);

//4. 98 – (dummy)mod97 operation:
long.TryParse(dummyAccount, out long dummyAccountInt);
int resultat = (int)(98 - dummyAccountInt % 97);
Console.WriteLine("Result operation : " + resultat);

//5.Assemble the IBAN number by appending BE + resultat operation + bank account number:
Console.WriteLine($"IBAN : BE{resultat}{compteToConvert}" );