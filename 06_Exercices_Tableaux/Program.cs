﻿// 1) Grâce à une boucle « while » et à l’aide d’une collection, calculez les nombres premiers inférieur à un nombre entier entré au clavier.
using System.Collections;

Console.WriteLine("Exo1 :");
int nombreExo1;
do
{
    Console.WriteLine("Entrez un nombre au clavier :");
} while (!int.TryParse(Console.ReadLine(), out nombreExo1));

ArrayList nbPrime = new ArrayList();
while (nombreExo1 > 1)
{
    bool isPrime = true;
    int sqrt = (int)Math.Sqrt(nombreExo1);
    for (int diviseur = 2; diviseur <= sqrt && isPrime; diviseur++)
    {
        // Test s'il est premier
        if (nombreExo1 % diviseur == 0)
        {
            isPrime = false;            
        }

    }
    if (isPrime)
    {
        // Si oui, on ajoute dans le tableau
        nbPrime.Add(nombreExo1);
    }

    nombreExo1--;
}
nbPrime.Sort();
foreach (int prime in nbPrime)
{
    Console.Write(prime + " | ");
}

// 2) Grâce à une boucle « for » et à l’aide d’une collection générique, calculez les x premiers nombres premiers (version optimisée).
Console.WriteLine("\n___________________________________________________");
Console.WriteLine("\nExo2 :");
int nombreExo2;
do
{
    Console.WriteLine("Entrez un nombre au clavier :");
} while (!int.TryParse(Console.ReadLine(), out nombreExo2));

List<int> nbPrime2 = new List<int>();
nbPrime2.Add(2);
for(int count  = 1, value = 3; count < nombreExo2; value++)
{
    bool isPrime = true;
    for(int i = 0; i < count && isPrime; i++)
    {
        if(value % nbPrime2[i] == 0)
        {
            isPrime = false;
        }
    }
    if (isPrime)
    {
        nbPrime2.Add(value);
        count++;
    }
}
Console.WriteLine();
foreach(int prime in nbPrime2)
{
    Console.Write(prime + " | ");
}

// 3) Demandez à l’utilisateur d’introduire deux nombres au clavier et faite l’addition de ces deux nombres en ne convertissant que caractère par caractère. (Méthode « ToCharArray() » de la classe « string »)
Console.WriteLine("\n___________________________________________________");
Console.WriteLine("\nExo3 :");
string nb1Ex3;
string nb2Ex3;
do
{
    Console.WriteLine("Entrez un premier nombre au clavier :");
    nb1Ex3 = Console.ReadLine();
}
while(!int.TryParse(nb1Ex3, out int osef1));
do
{
    Console.WriteLine("\nEntrez un second nombre au clavier :");
    nb2Ex3 = Console.ReadLine();
}
while (!int.TryParse(nb2Ex3, out int osef2));

char[] nb1Char = nb1Ex3.ToCharArray();
char[] nb2Char = nb2Ex3.ToCharArray();
List<int> resultat = new List<int>();

// Addition
//   1234
//    123

// TODO TROUVER L'ERREUR
bool retenue = false;
for (int i = 0; i < ((nb1Char.Length > nb2Char.Length)? nb2Char.Length : nb1Char.Length) ; i++)
{
    int add = (int)Char.GetNumericValue(nb1Char[nb1Char.Length - 1 - i]) + (int)Char.GetNumericValue(nb2Char[nb2Char.Length - 1 - i]);
    if(retenue)
    {
        add += 1;
        retenue = false;
    }
    if (add >= 10)
    {
        add -= 10;
        retenue = true;
    }
    resultat.Add(add);
}
for(int i = (nb1Char.Length > nb2Char.Length) ? nb1Char.Length - nb2Char.Length - 1: nb2Char.Length - nb1Char.Length - 1; i >= 0; i-- )
{
    if (retenue)
    {
        retenue = false;
        int temp = (nb1Char.Length > nb2Char.Length) ? (int)Char.GetNumericValue(nb1Char[i]) + 1 : (int)Char.GetNumericValue(nb2Char[i]) + 1;
        if(temp >= 10)
        {
            retenue = true;
            temp -= 10;
        }
        resultat.Add(temp);

    }
    else
    {
        resultat.Add((nb1Char.Length > nb2Char.Length) ? (int)Char.GetNumericValue(nb1Char[i]) : (int)Char.GetNumericValue(nb2Char[i]));

    }
}

Console.WriteLine("\nRésultat de l'addition :");
if(nb1Ex3.Length > nb2Ex3.Length)
{
    
    Console.Write("  "+nb1Ex3);
    Console.WriteLine();
    Console.Write("+ ");
    for (int i = 0; i < nb1Ex3.Length - nb2Ex3.Length; i++)
    {
        Console.Write(" ");
    }
    Console.Write(nb2Ex3);
    Console.WriteLine();
    Console.WriteLine("____________");
    Console.Write($" {(retenue ? "1" : " ") }");
    resultat.Reverse();
    foreach(int res in resultat)
    {
        Console.Write(res);
    }
}
else
{
    Console.Write("  ");
    for (int i = 0; i < nb2Ex3.Length - nb1Ex3.Length; i++)
    {
        Console.Write(" ");
    }
    Console.Write(nb1Ex3);
    Console.WriteLine();
    Console.Write("+ " + nb2Ex3);
    Console.WriteLine();
    Console.WriteLine("____________");
    Console.Write($" {(retenue ? "1" : " ")}");
    resultat.Reverse();
    foreach (int res in resultat)
    {
        Console.Write(res);
    }
}
Console.WriteLine();

// 4) Si vous voulez un bonus sympa visuellement et pas horrible :
// Créer un plateau de jeu de 10 x 10 et placer un pion quelque part dedans
// Tant que le joueur n'a pas appuyé sur Q, faire en sorte qu'il puisse aller à gauche, en haut, à droite ou en bas avec les flèches (hint : ReadKey)
// (attention, il se déplace que s'il peut, on ne peut pas sortir du plateau)

Console.ReadLine();
Console.Clear();

//Construction plateau
char[,] plateau = new char[10, 10];
for(int i = 0; i< plateau.GetLength(0); i++)
{
    for(int j = 0; j < plateau.GetLength(1); j++)
    {
        plateau[i, j] = '.';
    }
}
//Placement du p'tit pion
int posX = 2;
int posY = 1;
plateau[posY, posX] = 'P';

bool play = true;
// Jeu
do
{
    Console.Clear();
    //Affichage du plateau
    Console.WriteLine();
    for (int i = 0; i < plateau.GetLength(0); i++)
    {
        for (int j = 0; j < plateau.GetLength(1); j++)
        {
            Console.Write(" " + plateau[i, j]);
        }
        Console.WriteLine();
    }
    //Menu
    Console.SetCursorPosition(40, 5);
    Console.WriteLine("  ↑  ");
    Console.SetCursorPosition(40, 6);
    Console.WriteLine($"<-+->  pour se déplacer");
    Console.SetCursorPosition(40, 7);
    Console.WriteLine("  ↓  ");
    Console.SetCursorPosition(40, 9);
    Console.WriteLine("  Q    pour quitter");
    //Interaction utilisateur
    Console.SetCursorPosition(0, 12);
    Console.WriteLine("Appuyez sur une touche");
    ConsoleKeyInfo keyPressed = Console.ReadKey();
    switch (keyPressed.Key)
    {
        case ConsoleKey.UpArrow:
            if(posY > 0)
            {
                plateau[posY--, posX] = '.';
                plateau[posY, posX] = 'P';
            }
            break;
        case ConsoleKey.DownArrow:
            if (posY < plateau.GetLength(0) - 1)
            {
                plateau[posY++, posX] = '.';
                plateau[posY, posX] = 'P';
            }
            break;
        case ConsoleKey.LeftArrow:
            if (posX > 0)
            {
                plateau[posY, posX--] = '.';
                plateau[posY, posX] = 'P';
            }
            break;
        case ConsoleKey.RightArrow:
            if (posX < plateau.GetLength(1) - 1)
            {
                plateau[posY, posX++] = '.';
                plateau[posY, posX] = 'P';
            }
            break;
        case ConsoleKey.Q:
            play = false;
            break;
        default:
            Console.WriteLine("Invalid !"); // Qu'on voit pas, genius
            break;
    }

} while (play);