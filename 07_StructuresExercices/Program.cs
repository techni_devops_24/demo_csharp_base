﻿// 1) Écrire une structure pour définir un point possédant deux entier X, Y et créer un tableau deux dimensions de 5 sur 5 de type « Point » (nullable) et remplir une des diagonales, ensuite à l’aide de deux boucles afficher les valeurs dans la console:

using _07_StructuresExercices;

Point?[,] plateau = new Point?[5, 5];
//Boucle pour remplir la diag
for(int i = 0; i < plateau.GetLength(0); i++)
{
    for(int j = 0; j < plateau.GetLength(1); j++)
    {
        if(i == j)
        {
            plateau[i, j] = new Point() { X = j, Y = i};
        }
    }
}

//Boucle pour afficher
for (int i = 0; i < plateau.GetLength(0); i++)
{
    for (int j = 0; j < plateau.GetLength(1); j++)
    {
        if (plateau[i,j] is Point p)
        {
            Console.Write($"X : {p.X} - Y : {p.Y} |");
        }
        else
        {
            Console.Write("              |");
        }
    }
    Console.WriteLine();
}

// 2) Écrire deux structures Celsius et Fahrenheit toutes deux ayant une variable de type double appelée « Temperature ».
Celsius celsius;
celsius.Temperature = 13.00;

Fahrenheit far;
far.Temperature = 45.5;

Console.WriteLine($"Il fait actuellement {celsius.Temperature} ici");
Console.WriteLine($"Il fait actuellement {far.Temperature} là bas");