﻿using _08_Methodes;

Console.WriteLine("Les Méthodes :\n");
// Fonctions (return un type) et Procédures (void)

Person jm = new Person() { LastName = "Dupont", FirstName = "Jean-Mich", Birthdate = new DateOnly(1958, 5, 2) };
Person bb = new Person() { LastName = "Dupont", FirstName = "Bébé", Birthdate = DateOnly.FromDateTime(DateTime.Now) };
Person? boloss = null;
//boloss.Display(); //Nope parce qu'il y a un risque que ma variable soit nulle
boloss?.Display(); // ? vérifiera que la variable est bien non nulle et si c'est le cas, invoquera la méthode

jm.Display();
jm.Display("Plombier");
jm.Display("Policier", false);
bb.Display();

Console.WriteLine($"JM a {jm.Age()} et il est { (jm.IsAdult() ? "majeur" : "mineur") }");
Console.WriteLine($"BB a {bb.Age()} et il est { (bb.IsAdult() ? "majeur" : "mineur") }");

jm.SayHi("it");
jm.SayHi("en");
jm.SayHi("es");
jm.SayHi();
bb.SayHi("fr");

jm.FavHobbies("Pétanque", "Cinéma", "Armes");
//Avec param nommés (On peut choisir l'ordre, sinon c'est d'office l'ordre dans lequel ils sont déclarés dans la méthode)
jm.FavHobbies(hobbyN2: "Pétanque", hobbyN1: "Armes", hobbyN3: "Cinéma");

int nbPref = 12;
jm.FavNumber(ref nbPref); //in //ref
Console.WriteLine(nbPref);


//Params
jm.Jobs(false);
Console.WriteLine();
jm.Jobs(true, "Facteur");
Console.WriteLine();
jm.Jobs(false, "Pilote de ligne", "Chirurgien", "Pasteur");






