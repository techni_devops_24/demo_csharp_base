﻿// Demander à l’utilisateur d’encoder 1 nombre (int), si la somme des deux moitiés de celui-ci donne le nombre, afficher « le nombre est pair » sinon « le nombre est impair ».

//V1
Console.WriteLine("Veuillez entrer un nombre :");
if(int.TryParse(Console.ReadLine(), out int nombre))
{
    int moitie = nombre / 2;
    if(2*moitie == nombre)
    {
        Console.WriteLine($"Le nombre {nombre} est pair");
    }
    else
    {
        Console.WriteLine($"Le nombre {nombre} est impair");
    }
}
else
{
    Console.WriteLine("Vous n'avez pas entré un nombre");
}

//V2 - Avec modulo
Console.WriteLine("Veuillez entrer un nombre :");
if (int.TryParse(Console.ReadLine(), out int nombre2))
{
    //if( nombre2 % 2 == 0 )
    //{
    //    Console.WriteLine($"Le nombre {nombre2} est pair");
    //}
    //else
    //{
    //    Console.WriteLine($"Le nombre {nombre2} est impair");
    //}
    Console.WriteLine($"Le nombre {nombre2} est {((nombre2 % 2 == 0)? "pair" : "impair")}");

}
else
{
    Console.WriteLine("Vous n'avez pas entré un nombre");
}

// Le greed de la ternaire :
Console.WriteLine("Veuillez entrer un nombre :");
Console.WriteLine($"{( (int.TryParse(Console.ReadLine(), out int nombre3))? $"Le nombre {nombre3} est {((nombre3 % 2 == 0)? "pair" : "impair")}" : "Vous n'avez pas entré un nombre") } ");