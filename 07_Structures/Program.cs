﻿using _07_Structures;

Console.WriteLine("Les structures : ");

Person trainer1 = new Person("Strimelle", "Aurélien", new DateOnly(1989, 11, 1)); //Appel du constructeur
//trainer1.FirstName = "Aurélien";
//trainer1.LastName = "Strimelle";
//trainer1.Birthdate = new DateOnly(1989, 11, 1);

//Console.WriteLine($"Nous vous présentons {trainer1.FirstName} {trainer1.LastName} né(e) le {trainer1.Birthdate.ToShortDateString()}");
trainer1.Display();

Person trainer2;
trainer2.FirstName = "Khun";
trainer2.LastName = "Ly";
trainer2.Birthdate = new DateOnly(1982, 5, 6);

//Console.WriteLine($"Nous vous présentons {trainer2.FirstName} {trainer2.LastName} né(e) le {trainer2.Birthdate.ToShortDateString()}");
trainer2.Display();

//en gros c'est une classe avec moins de fonctionnalités. Si notre strucutre commence à avoir beaucoup des fonctionnalités d'une classe, autant en faire une classe.
//et surtout, on n'a pas d'héritage