﻿// Demander à l’utilisateur d’encoder 2 nombres (int) et d’en faire l’addition, la conversion devra utiliser la méthode « int.Parse() »
Console.WriteLine("Exercice 1 : Parse");
Console.WriteLine("Entrez un nombre :");
//string input1 = Console.ReadLine(); int nombre1 = int.Parse(input1)
int nombre1 = int.Parse(Console.ReadLine());
Console.WriteLine("Entrez un deuxième nombre :");
int nombre2 = int.Parse(Console.ReadLine());

int addition = nombre1 + nombre2;
Console.WriteLine($"{nombre1} + {nombre2} = {addition}");
//Console.WriteLine($"{nombre1} + {nombre2} = {nombre1 + nombre2}");

Console.WriteLine();

// Demander à l’utilisateur d’encoder 2 nombres (int) et d’en faire l’addition, la conversion devra utiliser la méthode « int.TryParse() »
Console.WriteLine("Exercice 2 : TryParse");

Console.WriteLine("Entrez un nombre :");
int.TryParse(Console.ReadLine(), out int nombre3);
//bool reussite1 = int.TryParse(Console.ReadLine(), out int nombre3);

Console.WriteLine("Entrez un deuxième nombre :");

int.TryParse(Console.ReadLine(), out int nombre4);

Console.WriteLine($"{nombre3} + {nombre4} = {nombre3 + nombre4}");
//en cas d'échec de conversion, il y aura 0 dans nos variables
//Idéalement il faudrait vérifier si ça a réussi pour afficher un résultat cohérent

Console.ReadLine();

