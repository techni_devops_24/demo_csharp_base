﻿// 1.Calculer les 25 premiers nombres de la suite de Fibonacci
// 0...1...1...2...3...5...8...13...21...34...55...89...144...233...
using System.Runtime.InteropServices;

Console.WriteLine("Exercice 1 : Fibonacci\n");
int temp1 = 0;
int temp2 = 1;
int result;

Console.Write(temp1 + " " + temp2 + " ");
for(int i = 2; i <= 25; i++)
{
    result = temp1 + temp2;
    temp1 = temp2;
    temp2 = result;
    Console.Write(result + " ");
}
Console.WriteLine();

// 2. Calculer la factorielle d’un nombre entré au clavier.
// 10! = 1 × 2 × 3 × 4 × 5 × 6 × 7 × 8 × 9 × 10 = 3 628 800
Console.WriteLine("\nExercice 2 : Factorielle\n");
Console.WriteLine("Entrez un nombre dont on va calculer la factorielle");
if(int.TryParse(Console.ReadLine(), out int facto))
{
    int res = 1;
    for (int i = 1; i <= facto; i++)
    {
        res *= i;
    }
    Console.WriteLine($"La factorielle de {facto} est {res}");

//    for(int i = facto-1; i > 0; i--)
//    {
//        facto *= i;
//    }
//    Console.WriteLine($"La factorielle est {facto}");
}
else
{
    Console.WriteLine("C'est pas un nombre");
}


// 3. Grâce à une boucle « for », calculez les x premiers nombre premier.
// 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
Console.WriteLine("\nExercice 3 : Nombres premiers\n");
Console.WriteLine("Entrez le nombre de nombres premiers que vous voulez :");
if (int.TryParse(Console.ReadLine(), out int nbPrem))
{
    //int i = 1;
    //int nbPremCompteur = 0;
    //while(nbPremCompteur < nbPrem)
    //{
    //    int compteur = 2;
    //    bool pasPremier = false;
    //    while(!pasPremier && compteur < i)
    //    {
    //        if(i%compteur == 0)
    //        {
    //            pasPremier = true;
    //        }
    //        compteur++;
    //    }
    //    if(!pasPremier)
    //    {
    //        Console.Write(i + " ");
    //        nbPremCompteur++;
    //    }
    //    i++;
    //}

    //Console.Write(1 + " ");
    //for(int i = 2; i <= nbPrem; i++)
    //{
    //    bool prem = true;
    //    for(int j = 2; j < i; j++)
    //    {
    //        if(i%j == 0)
    //        {
    //            prem = false;
    //            break;               
    //        }            
    //    }
    //    if(prem)
    //    {
    //        Console.Write(i + " ");
    //    }
    //}

        //Soluce by Thierry
        for (int count = 0, value = 2; count < nbPrem; value++)
        {
            bool isPrime = true;
            int sqrt = (int)Math.Sqrt(value);

            for (int diviseur = 2; diviseur <= sqrt && isPrime; diviseur++)
            {
                if(value % diviseur == 0)
                {
                   isPrime = false;
                }
            }// Détermine si le nombre est divisible par les nombres entre 2 et la racine du nombre

            if(isPrime)
            {
                Console.Write(value + " ");
                count++;
            }
        }

}
else
{
    Console.WriteLine("C'est pas un nombre");
}

// 4. A l’aide de boucles « for » afficher les 5 premières tables de multiplication en allant jusque « x20 ».
Console.WriteLine("\nExercice 4 : Tables\n");

for(int i = 1; i <= 5; i++)
{
    Console.WriteLine($"Table de {i} :");
    for (int j = 1; j <= 20; j++)
    {
        Console.WriteLine($"{i} * {j} = {i*j}");
    }
    Console.WriteLine();
}


// 5.À l’aide d’une boucle « for » comptez de 0, à 20,0 en augmentant de 0,1, en utilisant des doubles, et afficher la valeur à chaque itération.
// Remarquez-vous quelque chose de particulier ?
Console.WriteLine("\nExercice 5 : 0 to 20\n");
for (double i = 0; i < 20; i += 0.1)
{
    Console.WriteLine(i);
}

for (decimal i = 0; i < 20; i += 0.1M)
{
    Console.WriteLine(i);
}


// 6. Bonus : Calculer la racine carré d’un nombre avec maximum 10 décimales (Math.Sqrt(x) ne peut être utilisée que pour vérifier la réponse
Console.WriteLine("\nExercice 6 : L'enfer sur terre\n");

//Correction ©Kevin
double number = 144; 

double squareRoot = CalculateSquareRoot(number);
Console.WriteLine("la racine carré de {0} est approximativement : {1}", number, squareRoot);

static double CalculateSquareRoot(double number)
{
        // Initialisation de la racine carrée approximative 
        double sqrt = number / 2;

    // nombre maximal d'itérations 
    int maxIteration = 100;

    double epsilon = 1e-10; 

    //boucle d'itération pour améliorer l'approximation 
    for (int i = 0; i < maxIteration; i++)
    {
        //formule de la méthode de Newton pour la racine carrée 
        double nextSqrt = (sqrt + number / sqrt) / 2;

        // Vérifier la convergence en comparant avec la précision souhaitée
        if (Math.Abs(nextSqrt - sqrt) < epsilon)
        {
            return nextSqrt;
        }

        sqrt = nextSqrt;
    }
    return sqrt; // retourner la racine carrée approximative 
}

//Correction ©Clément
Console.WriteLine("Choisissez le nombre à sqrt");
double.TryParse(Console.ReadLine(), out double nombre);
double sqr = nombre, start = 0, end = nombre, tol = 0.0000000001;
while (Math.Abs(start - end) > tol)
{
    sqr = (start + end) / 2;
    if (sqr * sqr - nombre < 0)
        start = sqr;
    else
        end = sqr;
}
Console.WriteLine("Les racines sont = " + sqr + "et -" + sqr);
Console.WriteLine("Avec Math sqrt : " + Math.Sqrt(nombre));
Console.WriteLine($"Soit {Math.Sqrt(nombre) - sqr} de diff");