﻿// Les Enum :
using _09_Enumerations;

// Pour utiliser mon énum, je créé une variable du type de mon énum et lui attribue une des valeurs possibles de mon enum;
RoleDefault user1 = RoleDefault.User;
Console.WriteLine("User1 :");
Console.WriteLine(user1.ToString()); //Valeur littérale // User
Console.WriteLine((int)user1); //Valeur numérique // "index"

Console.WriteLine();

Role user2 = Role.User;
Console.WriteLine("User2 :");
Console.WriteLine(user2.ToString()); //Valeur littérale // User
Console.WriteLine((int)user2); //Valeur numérique // "index"

Console.WriteLine();
// On peut rajouter des flags sur nos énum, permettant ainsi d'avoir plusieurs valeurs ajoutées les unes aux autres
RoleFlags user3 = RoleFlags.User | RoleFlags.Moderator;
Console.WriteLine(user3.ToString()); //Valeur littérale // User
Console.WriteLine((int)user3); //Valeur numérique // "index"

RoleFlags user4 = RoleFlags.User | RoleFlags.Moderator | RoleFlags.Admin | RoleFlags.SuperAdmin;
Console.WriteLine(user4.ToString()); //Valeur littérale // User
Console.WriteLine((int)user4); //Valeur numérique // "index"

Console.WriteLine();
RoleFlags userToTest = user4;
if (userToTest.HasFlag(RoleFlags.User))
{
    Console.WriteLine("Vous êtes connecté, vous pouvez utiliser le site");
}
if (userToTest.HasFlag(RoleFlags.Moderator))
{
    Console.WriteLine("Vous êtes connecté, vous pouvez modérer le site");
}
if (userToTest.HasFlag(RoleFlags.Admin))
{
    Console.WriteLine("Vous êtes connecté, vous pouvez supprimer des posts et bloquer des utilisateurs");
}
if (userToTest.HasFlag(RoleFlags.SuperAdmin))
{
    Console.WriteLine("Vous êtes connecté, vous pouvez drop all");
}

// GetNames -> Pour récupérer toutes les valeurs littéralles existantes dans une enum
Console.WriteLine("\nValeurs de l'enum RoleFlags :");
foreach(string litt in Enum.GetNames(typeof(RoleFlags)))
{
    Console.WriteLine(litt);
}

// Parse -> pour passer d'une valeur litt à une valeur Enum
RoleFlags user5 = (RoleFlags)Enum.Parse(typeof(RoleFlags),"User");
//RoleFlags user5 = (RoleFlags)Enum.Parse(typeof(RoleFlags),"Visitor"); //Va planter puisque le role n'existe pas
if(user5.HasFlag(RoleFlags.User))
{
    Console.WriteLine("user5 est bien un user");
}

// TryParse -> Comme pour le int.TryParse, plus secure avec verification de réussite
if(Enum.TryParse<RoleFlags>("Visitor", out RoleFlags user6))
{
    Console.WriteLine("user6 créé");
}
else
{
    Console.WriteLine("Erreur conversion");
}