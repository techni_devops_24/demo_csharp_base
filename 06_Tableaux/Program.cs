﻿using System.Collections;
// 1) Les tableaux tableau
// Taille fixe, type donné

int[] notes = new int[10]; //Créé un tableau de 10 cases (vides)
int[] notes2 = { 12, 2, 8, 7, 15, 3 }; //Créé un tableau de 6 cases avec les valeurs renseignées
int[] notes3 = [1, 2, 3, 4, 5, 6]; //Créé un tableau de 6 cases avec les valeurs renseignées

// Pour accéder à une case en particulier, on utilise l'opérateur d'accès [ ]
// Rappel : La première case du tableau est à un l'index/indice 0 et la dernière taille-1
notes2[1] = 12; // -> Remplace la valeur dans la case à l'indice 1 donc 2 par 12 dans le tableau notes2

// Rappel boucle sympa avec les tableaux :
Console.WriteLine("Notes dans notes2 :");
foreach(int note in notes2)
{
    Console.Write(note + " | ");
}

// Tableaux à n dimensions
// On va déjà faire 2 hein
string[,] plateau = { { "0-0", "0-1", "0-2" }, { "1-0", "1-1", "1-2" } };

for(int i = 0; i < plateau.GetLength(0); i++)
{
    Console.WriteLine();
    for(int j = 0; j < plateau.GetLength(1) ; j++)
    {
        Console.Write(plateau[i,j] + " | ");
    }
}

// 2) Les collections
// La ArrayList : Tableau qui peut grandir
ArrayList meals = new ArrayList(); //Création de la liste, pas de taille vu que osef ça peut grandir

meals.Add("Pizza 4 Fromages"); //Ajouter UNE valeur
ArrayList favMeals = new ArrayList() { "Risotto", "Patates sous toutes ses formes", "Omelette" };
meals.AddRange(favMeals); //Ajouter plusieurs valeurs d'un coup
meals.Add(12); //Rappel : On peut ajouter n'importe quel type

Console.WriteLine("\nMenu 1 :");
//foreach(string meal in meals) //Va planter si y'a pas que des strings
foreach(var meal in meals)
{
    if (meal is string m)
    {
        Console.WriteLine(m.ToUpper());
    }
    if (meal is int mint)
    {
        Console.WriteLine(Math.Pow(mint, 2));
    }
}
Console.WriteLine("\nMenu 2 :");
meals.Remove("Pizza 4 Fromages");
foreach (var meal in meals)
{
    if (meal is string m)
    {
        Console.WriteLine(m.ToUpper());
    }
}

// Hashtable -> Dictionnaire clef - valeur
Hashtable menu = new Hashtable();

Console.WriteLine();
menu.Add("lundi", "Des patates");
menu.Add("mardi", "Des patates aussi");
menu.Add("mercrodi", "Oups je m'est trompé");
menu.Remove("mercrodi");
menu.Add(new DateOnly(1989, 11, 1), "Anniversaire Aurélien");

foreach (DictionaryEntry kp in menu)
{
    if(kp.Key is string key)
    {
        Console.WriteLine($"{key.ToUpper()} : {kp.Value}");
    }
    if(kp.Key is DateOnly keyDate)
    {
        Console.WriteLine($"{keyDate.ToShortDateString()} : {kp.Value}");
    }
}

// Queue
// FIFO (First In First Out)
Console.WriteLine();
Queue fileDAttente = new Queue(); 

Console.WriteLine("Jean-Michel arrive dans la file");
fileDAttente.Enqueue("Jean-Michel");
Console.WriteLine("Jeanne-Micheline arrive dans la file");
fileDAttente.Enqueue("Jeanne-Micheline"); 
Console.WriteLine("Georges arrive dans la file");
fileDAttente.Enqueue("Georges");
fileDAttente.Enqueue(12);

Console.WriteLine($"La prochaine personne sera... {fileDAttente.Peek()}");

Console.WriteLine("C'est bon, elle est passée");
fileDAttente.Dequeue(); //Jean-Michel s'en va

Console.WriteLine($"La prochaine personne sera... {fileDAttente.Peek()}");

// Stack
// LIFO (Last In First Out)
Stack bstorm = new Stack();

Console.WriteLine();
Console.WriteLine("Khun vient d'être recruté");
bstorm.Push("Khun");
Console.WriteLine("Aurélien vient d'être recruté");
bstorm.Push("Aurélien");
Console.WriteLine("Aude vient d'être recrutée");
bstorm.Push("Aude");
bstorm.Push(DateTime.Now);

Console.WriteLine($"Mince restriction de budget, on va devoir virer {bstorm.Peek()}");

Console.WriteLine("Ayé c'est dégagé");
bstorm.Pop();
Console.WriteLine($"Mince encore restriction de budget, on va devoir virer {bstorm.Peek()}");

// 3) Les collections génériques
// ArrayList -> List
Console.WriteLine("\nList<T>\n");
List<string> mealList = new List<string>();
mealList.Add("Pizza Hawaï");
//mealList.Add(12); //NOPE
mealList.AddRange(new List<string>() { "Raclette", "Fondue", "Burger" });
mealList.Remove("Pizza Hawaï");

Console.WriteLine("Taille de la liste : " + mealList.Count());
foreach(string item in mealList) 
{
    //Plus besoin de mettre var dans le foreach et de faire les verif de type puisque c'est d'office des strings ici
    Console.WriteLine(item);
}

// Hashtable -> Dictionary
Console.WriteLine("\nDictionary<T,U>\n");

Dictionary<string, DateOnly> birthdays = new Dictionary<string, DateOnly>();

//birthdays.Add(12, true); //NOPE
birthdays.Add("Khun", new DateOnly(1982, 5, 6));
birthdays.Add("Aurélien", new DateOnly(1989, 11, 1));
birthdays.Add("Aude", new DateOnly(1989, 10, 16));

birthdays.Remove("Aude");

foreach(KeyValuePair<string, DateOnly> kvp in birthdays)
{
    //On a typé kvp pour indiquer que la clef est bien un string et la valeur un DateOnly, donc pas de vérif à faire et les méthodes de chaque type sont bien dispo
    Console.WriteLine($"{kvp.Key.ToUpper()} : {kvp.Value.ToShortDateString()}");
}

//Queue -> Queue (mais générique)
Console.WriteLine("\nQueue<T>\n");
Queue<string> listeAttente = new Queue<string>();

//listeAttente.Enqueue(12); //NOPE
listeAttente.Enqueue("Jean-Mich");
listeAttente.Enqueue("Jeanette");
Console.WriteLine("Prochain à partir : " + listeAttente.Peek());
listeAttente.Dequeue();
listeAttente.TryPeek(out string peeked);
Console.WriteLine("Prochain à partir : " + peeked);

//Stack -> Stack (mais générique)
Console.WriteLine("\nStack<T>\n");
Stack<string> assiettes = new Stack<string>();

assiettes.Push("Assiette1");
assiettes.Push("Assiette2");
assiettes.Push("Assiette3");
Console.WriteLine("La prochaine assiette à partir sera : " + assiettes.Peek());
assiettes.Pop();
Console.WriteLine("La prochaine assiette à partir sera : " + assiettes.Peek());



