﻿Console.WriteLine("Les conversions :");
Console.WriteLine();

//--------- De Type valeur -> String ---------
int entier = 4;
Console.WriteLine(entier.ToString());
double monDouble = 12.6;
Console.WriteLine(monDouble.ToString());
bool monBool =  false;
Console.WriteLine(monBool.ToString());

//--------- De String -> Type Valeur ----------
string chaineEntier = "15";
string chaineDouble = "12.56";
string chaineDouble2 = "12,56";
string chaineChaine = "Banane";

// CONVERT
Console.WriteLine(Convert.ToInt32(chaineEntier));
//Console.WriteLine(Convert.ToInt32(chaineDouble)); //Va faire planter le programme
//Console.WriteLine(Convert.ToInt32(chaineDouble2)); //Va faire planter le programme

Console.WriteLine(Convert.ToDouble(chaineEntier));
Console.WriteLine(Convert.ToDouble(chaineDouble));
Console.WriteLine(Convert.ToDouble(chaineDouble2));

// PARSE
Console.WriteLine( int.Parse(chaineEntier) );
//Console.WriteLine( int.Parse(chaineDouble) ); //nope
//Console.WriteLine( int.Parse(chaineDouble2) ); //nope

Console.WriteLine( double.Parse(chaineDouble) ); //passe mais pas de virgule
Console.WriteLine( double.Parse(chaineEntier) ); 
Console.WriteLine( double.Parse(chaineDouble2) );

// TRYPARSE -> Celle que vous allez privilégier
//int result;
//bool reussite = int.TryParse(chaineEntier, out result);
bool reussite = int.TryParse(chaineEntier, out int result);
Console.WriteLine(reussite + " : " + result);
reussite = int.TryParse(chaineDouble, out result);
Console.WriteLine(reussite + " : " + result);
reussite = int.TryParse(chaineDouble2, out result);
Console.WriteLine(reussite + " : " + result);

bool reussite2 = double.TryParse(chaineEntier, out double result2);
Console.WriteLine(reussite2 + " : " + result2);
reussite2 = double.TryParse(chaineDouble.Replace('.', ','), out result2);
Console.WriteLine(reussite2 + " : " + result2);
reussite2 = double.TryParse(chaineDouble2, out result2);
Console.WriteLine(reussite2 + " : " + result2);

// Try Parse -> Ne provoquera jamais d'erreurs
// ---------- Conversion explicite ------
int nbFichiers = 208;
int nbFichierCopies = 53;

int pourcentage = (int)((double)100 / nbFichiers * nbFichierCopies);
//(type) -> Conversion explicite

Console.WriteLine(pourcentage + " %");

//-------- Boxing/Unboxing ---------
// C'est faire la conversion implicite/explicite entre objet et type valeur
int j = 12;
object obj = j; //Boxing -> Conversion implicite (Mon entier est aussi un objet)
int k = (int)obj; //Unboxing -> Conversion explicite (Je précise que mon objet est un entier)




