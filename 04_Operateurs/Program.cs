﻿//---------- LES OPÉRATEURS
// Les opérateurs logiques
// == != >= > < <= ! && || ^

// Les opérateurs arithmétiques
// + - * / % 

// Les opérateurs d’affectations et raccourcis
// =
int x = 5;
x = x + 12; //same as ↓
x += 12;
x -= 2;
// dispo pour tous les opérateurs += -= *= /= %= 
int p = (int)Math.Pow(2, 6);
Console.WriteLine(p);

// Pré et Post incrémentation (décrémentation)
int i = 0;
int j = 0;

Console.WriteLine("i : ", i++); //0
Console.WriteLine("j : ", ++j); //1

// L’opérateur ternaire
// (condition) ? valeur si condition ok : valeur si condition pas ok
// Voir demo condition

// L’opérateur coalesce et affectation
string chienGavin = "Skye";
string chienAude = null;

Console.WriteLine($"Le chien de Gavin s'appelle {chienGavin ?? "pas"}");
Console.WriteLine($"Le chien de Aude s'appelle {chienAude ?? "pas"}");
// Pourrait s'écrire avec une ternaire mais c'est plus long
Console.WriteLine($"Le chien de Aude s'appelle {( chienAude is null ? "pas" : chienAude)}");

//Affectation
// N'affectera à la variable que si elle est null
chienGavin ??= "Jean-Luc";
chienAude ??= "Jean-Luc";
Console.WriteLine($"Le chien de Gavin s'appelle {chienGavin}");
Console.WriteLine($"Le chien de Aude s'appelle {chienAude}");

// L’opérateur « typeof » et GetType
Console.WriteLine( typeof(object) );
Console.WriteLine( typeof(int) );
Console.WriteLine( typeof(string) );
Console.WriteLine( typeof(DateTime) );

int monEntier = 12;
string hobby = "Twitch";
DateTime today = DateTime.Now;
object myojb = new object();
Console.WriteLine();
Console.WriteLine(monEntier.GetType());
Console.WriteLine(hobby.GetType());
Console.WriteLine(today.GetType());
Console.WriteLine(myojb.GetType());
myojb = monEntier;
Console.WriteLine(myojb.GetType());

// L’opérateur « is »
// Permet de savoir si notre variable est compatible avec un type donné (ou une interface, on verra ça plus tard)
object boite;
int unNombre = 21;
boite = unNombre;
if(boite is int)
{
    Console.WriteLine("La boîte est un entier");
}

if(boite is object)
{
    Console.WriteLine("La boîte est un objet");
}

if (boite is string)
{
    Console.WriteLine("La boîte est une chaine");
}

// Pattern matching
object boite2;
int unNombre2 = 21;
boite2 = unNombre2;
if (boite2 is int)
{
    int miniBoite = (int)boite2; //sans matching pattern
    Console.WriteLine("La boîte est un entier");
}

if (boite2 is object nouvelleBoite) //avec matching pattern : je ne dois plus déclarer ma variable et faire un casting explicite
{
    Console.WriteLine(nouvelleBoite);
    Console.WriteLine("La boîte est un objet");
}

if (boite2 is string)
{
    Console.WriteLine("La boîte est une chaine");
}

string chaine1 = "coucou";
string chaine2 = "coucou";
if(chaine1 == chaine2)
{
    Console.WriteLine("Les deux chaines sont égales (==)");
}
if(chaine1.Equals(chaine2))
{
    Console.WriteLine("Les deux chaines sont égales (Equals)");
}

// L’opérateur « as »
object obj = DateTime.Now;
//object obj = "DateTime.Now";
//string couc = (string)obj; //Casting explicite 
//Rate si pas les bons types
//Console.WriteLine("\n" + couc);
string couc2 = obj as string;
int? couc3 = obj as int?;
Console.WriteLine(couc2 ?? "Casting raté avec as");
