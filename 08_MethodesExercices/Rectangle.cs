﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_MethodesExercices
{
    public struct Rectangle
    {
        public double Longueur, Largeur;

        public double CalculAire()
        {
            return Longueur * Largeur;
        }
    }
}
