﻿
namespace _09_Enumerations
{
    public enum RoleDefault
    {
        User, //0
        Moderator, //1
        Admin //2
    }

    public enum Role
    {
        User = 45, //45
        Moderator = 152, //152
        Admin = 3 //3
    }

    [Flags]
    public enum RoleFlags
    {
        User = 1,
        Moderator = 2,
        Admin = 4,
        SuperAdmin = 8
    }
}
