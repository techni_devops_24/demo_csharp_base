﻿namespace _09_EnumerationsExercice
{
    public struct Carte
    {
        public CouleursEnum Couleur;
        public ValeurEnum Valeur;
    }
}
