﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_MethodesExercices
{
    public struct Cercle
    {
        public double Rayon;
        public double CalculAire()
        {
            return Math.PI * Math.Pow(Rayon, 2);
        }
    }
}
