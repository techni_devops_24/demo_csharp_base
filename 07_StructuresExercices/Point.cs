﻿namespace _07_StructuresExercices
{
    public struct Point
    {
        public int X;
        public int Y;
    }
}
