﻿#region Ecrire/Lire/Nettoyer + SetCursor et changerCouleur
Console.WriteLine("Entrez votre nom");
string nom = Console.ReadLine();
Console.WriteLine("Bonjour " + nom);

Console.WriteLine("Tapez sur n'importe quelle touche pour nettoyer la console");
Console.ReadLine();
Console.Clear();

//Console.SetCursorPosition(50, 50);
//Console.BackgroundColor = ConsoleColor.Green;
//Console.ForegroundColor = ConsoleColor.Magenta;
//Console.WriteLine("Coucou");

string firstname = "Aude";
string lastname = "Beurive";
int age = 34;
Console.WriteLine("Bonjour {0} {1}, vous avez {2} ans", firstname, lastname, age);
// Avec l'interpolation
Console.WriteLine($"Bonjour {firstname} {lastname}, vous avez {age} ans");

Console.ReadLine();
#endregion

#region Les petits tips
//1) Pour activer le retour à la ligne auto : 
//Outils -> Options -> Editeur de texte -> C# -> General -> Cocher retour auto
//2) Commenter : Ctrl+K+C
//3) Décommenter : Ctrl+K+U
//4) Le tips contre Aurélien : Windows + L
//5) Panel Emoji + carac spéciaux : Windows + ;
#endregion

#region Les variables
// ------- DECLARATION -------
// type nomDeVariable
//int nombre;
// convention de nommage :
// lowerCamelCase pour les variables/champs
// UpperCamelCase pour les méthodes/prop/etc
// Pas de caractères spéciaux (sauf _ mais il a une signification), pas d'accents
// On essaie de les nommer en anglais et de trouver des noms qui sont à la fois courts mais significatifs
// Chiffres autorisés mais pas en premier caractère

//--------- INITIALISATION ---------
// = Sert à affecter une valeur à une variable
// La première affectation s'appelle l'initialisation
int nombre;
string chaine = "une chaine"; //Soit sur la même ligne que la décla
DateTime date; 

nombre = 12; //Soit plus loin (mais alors faut pas oublier)

Console.WriteLine("Nombre : ", nombre);
Console.WriteLine("Chaine : ", chaine);
//Console.WriteLine("Date : ", date); // Marche pas, pas init

// Portée des variables : une variable n'est disponible que dans le bloc {} de code dans lequel elle est déclarée et tous les blocs à l'intérieur de ce dernier
//int truc = 23;
//for(int i=0; i< 10; i++)
//{
//    int pouet = 12;
//    Console.WriteLine(i); //oui
//    Console.WriteLine(pouet); //oui
//    Console.WriteLine(truc); //oui
//}
//Console.WriteLine(i); //non
//Console.WriteLine(pouet); //non
//Console.WriteLine(truc); //oui

//--------- CONSTANTES ---------
// 1) Même règles de convention de nommage que les variables sauf qu'elles doivent être en maj
// 2) Doivent être déclarées et initialisées en même temps
const double PI = 3.14;
//PI = 12; impossible

// Types nullables ou non
// Certaines types accepteront d'office la valeur null, d'autres pas
// Pour ceux qui n'acceptent pas la valeur null, on pourra rajouter un petit signe indiquant qu'en fait si
string hobby = null; //ok
//int nombrePref = null; //pas ok
int? nombrePref2 = null;

// --------- Les Types -----------
// Les types valeurs
int monEntier = 2;
double monDouble = 2.56;
char monCharactere = 'a';
bool monBool = false;

// Les types références
string maChaine = "maChaine";
object monObjet = new object();

// Différence entre les deux :
int x = 5;
int y = x;
x = 6;
// x = 6 et y = 5  // Nous avons copié la valeur de x dans y

string z = "pouet";
string w = z; //Actuellement elles partagent la même référence
Console.WriteLine(ReferenceEquals(w, z));
z = "mlem"; //On créé une nouvelle référence pour z
Console.WriteLine(ReferenceEquals(w, z));

Console.WriteLine();

//int[] tab1 = [1, 2, 3, 4];
//int[] tab2 = tab1;
//tab2[1] = 12;
//Console.WriteLine(tab1[1] + " " + tab2[1]); //12 et 12

var maVar = 5;
var maVar2 = "oui."; //Prend le type au moment de l'initialisation
#endregion