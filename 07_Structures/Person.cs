﻿
namespace _07_Structures
{
    public struct Person
    {
        public string LastName, FirstName;
        public DateOnly Birthdate;

        //Constructeur (on le revoit en détail en OO)
        public Person(string ln, string fn, DateOnly bd)
        {
            LastName = ln;
            FirstName = fn;
            Birthdate = bd;
        }

        //Méthode (on revoit dans OO)
        public void Display()
        {
            Console.WriteLine($"Nous vous présentons {FirstName} {LastName} né(e) le {Birthdate.ToShortDateString()}");
        }
    }
}
