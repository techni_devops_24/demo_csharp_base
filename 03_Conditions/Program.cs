﻿//------ CONDITIONS ------
// if...else  / if..else...if..else...
//if (condition)
//{
//    instructions si condition vraie
//}
//else
//{
//    instructions si condition fausse
//}

Console.WriteLine("Entrez votre âge :");
int.TryParse(Console.ReadLine(), out int age);
if(age >= 18)
{
    Console.WriteLine("Bienvenue sur notre site ;)");
}
else
{
    Console.WriteLine("Hep ! C'est interdit /!\\ ");
}

// switch
Console.WriteLine("Quel jour de la semaine sommes-nous ? ");
int.TryParse(Console.ReadLine(), out int jour);
switch (jour)
{
    case 1:
        Console.WriteLine("C'est lundi");
        break;
    case 2:
        Console.WriteLine("C'est mardi");
        break;
    case 3:
        Console.WriteLine("C'est mercredi");
        break;
    case 4:
        Console.WriteLine("C'est jeudi");
        break;
    case 5:
        Console.WriteLine("C'est vendredi");
        break;
    case 6:
    case 7:
        Console.WriteLine("C'est le weekend");
        break;
    default:
        Console.WriteLine("Bah c'est juste pas un jour valide en fait");
        break;
}

// Ternaire
int qtBananes = 1;
int qtPatates = 5;
//if(qtBananes > 1)
//{
//    Console.WriteLine($"Il y a {qtBananes} bananes dans le panier");
//}
//else
//{
//    Console.WriteLine($"Il y a {qtBananes} banane dans le panier");
//} //chiant donc ->

// (condition)? si oui : si non
Console.WriteLine($"Il y a {qtBananes} banane{((qtBananes > 1)? "s" : "")} dans le panier");
Console.WriteLine($"Il y a {qtPatates} patate{((qtPatates > 1)? "s" : "")} dans le panier");


//Svp faites pas ça
Console.WriteLine($"C'est ${((jour == 1)? "Lundi" : (jour == 2)? "Mardi" : (jour == 3) ? "Mercredi" : (jour == 4)? "Jeudi" : "fin de semaine" )}");