﻿
namespace _08_Methodes
{
    public struct Fahrenheit
    {
        public double Temperature;

        public double ToCelsius()
        {
            return (Temperature - 32) * 9 / 5;
        }
    }
}
